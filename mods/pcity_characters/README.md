# Licenses for textures, models and sounds

## Copyright © 2024 Jan Wielkiewicz
  CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

* every texture, model, sound unless stated otherwise

## CC0, public domain

* pcity\_characters\_palette.png (a composite of GIMP palettes, https://gitlab.gnome.org/GNOME/gimp/-/blob/master/LICENSE)
