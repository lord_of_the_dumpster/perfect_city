# Licenses for textures, models and sounds

## Copyright © 2024 Jan Wielkiewicz
  CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

* every texture, model, sound unless stated otherwise

## Not covered by copyright?

* pcity\_characters\_palette.png (a composite of GIMP palettes)
