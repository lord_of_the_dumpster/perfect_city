# Licenses for textures, models and sounds

## Copyright © 2023-2024 Jan Wielkiewicz
  CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

* every texture, model, sound unless stated otherwise


## Copyright © 2024 TubberPupper
  CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

* pcity_nodes_grass_footstep.2.ogg
* pcity_nodes_grass_footstep.3.ogg
* pcity_nodes_grass_footstep.4.ogg
* pcity_nodes_grass_footstep.5.ogg
* pcity_nodes_grass_footstep.6.ogg
* pcity_nodes_grass_footstep.7.ogg
* pcity_nodes_grass_footstep.ogg
* pcity_nodes_hard_footstep.2.ogg
* pcity_nodes_hard_footstep.3.ogg
* pcity_nodes_hard_footstep.ogg
