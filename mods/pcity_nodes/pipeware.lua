--[[
    This is a part of "Perfect City".
    Copyright (C) 2024 Jan Wielkiewicz <tona_kosmicznego_smiecia@interia.pl>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]

local mod_name = minetest.get_current_modname()

local color_palette = "pcity_common_palette.png"

minetest.register_node(
    mod_name..":tap_up",
    {
        drawtype = "mesh",
        mesh = mod_name.."_tap_up.obj",
        description = "Tap up",
        tiles = {
            {name = mod_name.."_metal.png"},
            {name = color_palette}},
        groups = {cracky = 3, stone = 1},
        paramtype = "light",
        paramtype2 = "wallmounted",
        wallmounted_rotate_vertical = true,
    }
)

minetest.register_node(
    mod_name..":tap_down",
    {
        drawtype = "mesh",
        mesh = mod_name.."_tap_down.obj",
        description = "Tap down",
        tiles = {
            {name = mod_name.."_metal.png"},
            {name = color_palette}},
        groups = {cracky = 3, stone = 1},
        paramtype = "light",
        paramtype2 = "wallmounted",
        wallmounted_rotate_vertical = true,
    }
)

minetest.register_node(
    mod_name..":sink",
    {
        drawtype = "mesh",
        mesh = mod_name.."_sink.obj",
        description = "Sink",
        tiles = {
            {name = mod_name.."_metal.png"},
            {name = color_palette},
        },
        groups = {cracky = 3, stone = 1},
        paramtype = "light",
        paramtype2 = "wallmounted",
        wallmounted_rotate_vertical = true,
    }
)

minetest.register_node(
    mod_name..":toilet_open",
    {
        drawtype = "mesh",
        mesh = mod_name.."_toilet_open.obj",
        description = "Toilet open",
        tiles = {
            {name = color_palette},
        },
        groups = {cracky = 3, stone = 1},
        paramtype = "light",
        paramtype2 = "4dir",
    }
)

minetest.register_node(
    mod_name..":toilet_closed",
    {
        drawtype = "mesh",
        mesh = mod_name.."_toilet_closed.obj",
        description = "Toilet closed",
        tiles = {
            {name = color_palette},
        },
        groups = {cracky = 3, stone = 1},
        paramtype = "light",
        paramtype2 = "4dir",
    }
)
