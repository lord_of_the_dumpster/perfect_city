pcity_nodes = {}

-- Load files
local path = minetest.get_modpath("pcity_nodes")

dofile(path.."/nodes.lua")
dofile(path.."/nature.lua")
dofile(path.."/walls.lua")
dofile(path.."/streets.lua")
dofile(path.."/street_lights.lua")
dofile(path.."/pipeware.lua")
dofile(path.."/furniture.lua")
dofile(path.."/shelves.lua")
dofile(path.."/lamps.lua")
