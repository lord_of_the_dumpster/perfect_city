# Licenses for textures, models and sounds

## Copyright © 2023-2024 Jan Wielkiewicz
  CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

* pcity_sky.xcf
* pcity_environment_down.png
* pcity_environment_crane_monster.png
* pcity_environment_smog.png
* pcity_environment_city.png
* pcity_environment_noise.png
* pcity_environment_cranes_right.png
* pcity_environment_cranes_left.png
* pcity_environment_cranes_front.png
* pcity_environment_cranes_back.png
* pcity_environment_chimneys_left.png
* pcity_environment_power_lines.png
* pcity_environment_silos.png
* pcity_environment_horizon.png
* pcity_environment_base.png
* pcity_environment_base_blue.png
* pcity_environment_base_black.png
