# Licenses for textures, models and sounds

## Public domain

- kopernik.jpg
- Flammarion.jpg

## Copyright © 2024 Jan Wielkiewicz
  CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

Yeah, I'm terrible at drawing!

- tap_monster.png
- drill_man.png
- claus.jpg
- roadsign_monster.png
- chimera.jpg
