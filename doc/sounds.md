# A list of sounds that could be useful:

## Noise, Horror:
https://freesound.org/people/looplicator/sounds/738676/

https://freesound.org/people/Jovica/sounds/6616/

https://freesound.org/people/Sclolex/sounds/180497/

https://freesound.org/people/waveplaySFX/sounds/187518/

https://freesound.org/people/waveplaySFX/sounds/554867/

https://freesound.org/people/eliwynnmusic/sounds/513184/

https://freesound.org/people/DJScreechingPossum/sounds/695879/

https://freesound.org/people/IanStarGem/sounds/269597/

https://freesound.org/people/UNIVERSFIELD/sounds/742851/

https://freesound.org/people/UNIVERSFIELD/sounds/742850/

https://freesound.org/people/UNIVERSFIELD/sounds/740204/

https://freesound.org/people/newlocknew/sounds/584952/

## Weather:

https://freesound.org/people/Vrymaa/sounds/735350/

https://freesound.org/people/Vrymaa/sounds/734931/

https://freesound.org/people/DWOBoyle/sounds/136692/

## Ambience:

https://freesound.org/people/OnlyTheGhosts/sounds/251620/

https://freesound.org/people/OnlyTheGhosts/sounds/251624/

https://freesound.org/people/RandomRecord19/sounds/736237/

https://freesound.org/people/Vrymaa/sounds/735353/

https://freesound.org/people/Vrymaa/sounds/734642/

https://freesound.org/people/Vrymaa/sounds/734642/

https://freesound.org/people/Rico_Casazza/sounds/691025/

https://freesound.org/people/klankbeeld/sounds/325442/

https://freesound.org/people/cormi/sounds/85086/

https://freesound.org/people/conleec/sounds/159740/

https://freesound.org/people/talithamaree_2L/sounds/593575/

https://freesound.org/people/jimmygian/sounds/473616/

https://freesound.org/people/magnus589/sounds/379738/

https://freesound.org/people/lolamadeus/sounds/161223/

https://freesound.org/people/klankbeeld/sounds/469073/

https://freesound.org/people/more7859/sounds/489401/

https://freesound.org/people/klankbeeld/sounds/576588/

https://freesound.org/people/klankbeeld/sounds/379315/

https://freesound.org/people/klankbeeld/sounds/622190/

## Weird:

https://freesound.org/people/deadrobotmusic/sounds/629042/

https://freesound.org/people/Nadia4Sound/sounds/424848/

https://freesound.org/people/UNIVERSFIELD/sounds/697082/

https://freesound.org/people/UNIVERSFIELD/sounds/738235/
