**CAUTION: This document contains SPOILERS!**

# The City

The Perfect City is a dream/nightmare world mostly devoid of people
where Seba is trapped after his girlfriend gets kidnapped.  The City
appears to have its own will, but readily responds to Seba's mental
state - his mind and the City are one thing. The origin of the City is
uncertain and mysterious.  Some characters including Seba have
god-like abilities, yet are not omnipotent.  I think therefore I am?
Or maybe I think because I am? What am I? Where am I?  I think because
there's the City, the nightmare that won't let me wake up.  If you
can't wake up that nightmare might as well be your reality...

## 0. Core elements of the City

### 0.1 Maze

All buildings look familiar, roads lead to nowhere, it's easy to get
lost.  The city wants you to lose your way home, your belongings and
soul.

### 0.2 Empty, lonely

The City is uncannily empty, looks like people left few moments ago
but never returned.  With people gone the City keeps living like a
corpse walking. There are few people in the City, most of them insane,
unfamiliar or disappear in moments you need them most.  Long dead,
insane, imagined, these are the citizens of the Perfect City.

### 0.3 Dream-like

The teapot talks! The walls listen! Is the world fucked up or am I
fucked up?  You can almost grab the moon, it's so close! You never
know if you're awake or dreaming.

### 0.4 Insane

Seba's mind melts, the insanity of the City grows on him. I closed the
doors, did I?  I left the doors here, but they're gone. There's only
the key left in the concrete wall.  Wait, the doors were in my pocket
this entire time! Silly me. The key melts.

### 0.5 Stalks you

There are no people but you feel watched. The items move, looking
askance at you.  Something moved in the other room. Was this street
light here last time?  The city plays around with you, it sees your
moves and acts to get you lost.

### 0.6 Dark

With barely any color, the world is gray. The TV noise pours into your
eyes and drains your brain.  You think you're over, your previous life
lost, you're thinking about ending it all.  It's high up there, the
cold wind is blowing in your face and even after all your tears dried
up the rain still fills your eyes.

### 0.7 Mysterious

What's behind the corner? Where are the pipes leading to? Why am I
trapped here?  Where's my girlfriend? Is there a god of this world?

### 0.8 Hostile

The City wants to steal your soul and eat your flesh. It's nothing
personal, is it?  If this is hell, then the City's hostility is your
punishment, but it's quite rough even for hell.

### 0.9 Strips your identity

As you stay in the City you lose a sense of self and start questioning
your very existence.  The City wants to remake Seba in its own image.

### 0.10 Hope, Beauty

Even in all this madness Seba still loves the City because **it's
where he belongs.**  Seba loves the smell of warm asphalt during rain,
loves the walls of his ghetto.  Monsters chasing him won't make him
give up on his friends. With or without his soul, he still loves and
cares.

### 0.11 Nostalgia for the place you've never been to

Even though you've never been to this place, you feel nostalgic.  The
Perfect City is filled with Seba's memories and places that ceased to
exist or never existed.  And yet you feel just like at home, meeting
your dearest friend.

### 0.12 Layers

The city has multiple layers, both physical and psychological.
Physical layers of the city are: buildings, the outside, the basement
and the hell (the bottom part of the basement). Psychological layers
are: the "real" world (the one Seba was before entering the city, the
city, Seba's memories, dreams and nightmares. Although the layers
exists, their boundaries aren't clear and often get blurred. Seba's
thoughts and nightmares soak into the city's tissue.

## 1. Gameplay

The two most basic features of Perfect City's gameplay are survival
and exploration, which are interconnected. The player lands in the
city, needs to look for food and find shelter. In order to do that
they're forced to explore. Exploration should be triggered by
physiology and dangers of the city, but also by the player's
curiosity. The game should reward observation and gaining experience
by making some dangers avoidable and unlocking new possibilities.

### 1.1 Homelessnes

You were thrown into the Perfect City by supernatural forces, it's
your city, but there are few places that you can actually call
home. The city makes you feel unwelcome and anxious. Endlessly stuck
between stranger flats and buildings, looking for a place you can call
home.  The game should force the player to change shelters often by
means such as physiology, monsters or changing the building's
layout. Extending usability of a shelter should be possible by taking
longer trips out, using lucky charms, etc.

### 1.2 Seba's Memories

Seba's memories from the "real" life are scattered all around the
city. Collecting them enables the story's progression. Memories are
activated when Seba sees a familiar place, item or a
person. Collecting a memory makes the boundary between his memories
and the city's reality blurry. Seba is forced to redo a scene from his
memories. Completing the task correctly unlocks a reward (e.g. an
item).

### 1.3 Variable environment

The city should be dynamic. Days should be safer for exploration, but
not completely safe. Night is the time of monsters, the player should
find a shelter before that time, otherwise finding the way back may
become tricky. During the night supernatural things happen, the city
shifts the layout more often and monsters have an advantage over
you. The night/day cycle is variable and sometimes the city can trick
the player into staying out too long. The weather is all shades of
gray and cloudy; wind, rain, snow or fog, no sun allowed.

### 1.4 The story

The game's lore revolves around Seba's "real" life, his quest to find
his girlfriend who disappeared mysteriously. Interactions with
Copernicus, Alex and other characters should make the game interesting
and chaotic but should also be a counterbalance to the city's dark
nature. The final point of the story should be getting "out" of the
city.

### 1.5 Escaping Monsters

Monsters in the game should be generally stronger than Seba and almost
impossible to defeat. The only way to fight them is by playing their
rules, outsmarting them and running away.

### 1.6 Bad omens, lucky charms

Bad omens are phenomena that foretell the future. Bad omens precede
the arrival of a monster and are environmental cues that the player
can read to avoid the danger. Bad omens take shape of things that
"feel off", for example an item moves in a room, changes position,
color or appears in a bizarre place (e.g. a door knob in a wall). Bad
omens would also involve changing the layout of the building the
player is currently in.

Lucky charms on the other hand are items that ward off dangers or
increase luck. Lucky charms can be obtained from other characters,
found randomly or crafted with varying efficacy. They often take a
form of items that feel otherwise useless, unimportant.

## 2. Monsters

Monsters of the Perfect City are personifications of Seba's fears and
traumas.

### 2.1 Tap monster

While being at grandma's Seba was always afraid of that bathroom tap.
The tap looks like it's staring at you and your late-night toilet
visit is acompanied by the sound of dripping water.  You're afraid to
look away so you keep eye contact with it but as you do so the tap
drills you with its sight.  The water it produces is a green poison of
madness, those who drink it have their minds controlled by the tap.
The tap monster can suddenly appear in strange places, it will try
tricking you into drinking its poison, but if it fails it'll break
free out of the wall and start chasing you.  If the tap catches you it
will vomit the poison straight into your throat.

### 2.2 Drillman

### 2.3 TV monster

Kids these days stare too much at TV, Seba was no different.  Every
time his parents had an argument Seba would shut in his room and turn
up the volume so he doesn't hear them.  One day it got really
quiet. Seba turned off the TV and left his room to check what's
happening.  His parents were gone, the home was strangely dark and
quiet. Seba closed his eyes and had a horrid vision.  He rushed back
into his room and turned on the TV, but it screamed at him with noise
instead.  "I'd like my parents to be like this TV" was the thought
Seba had back then. He imagined his parents with their heads displaced
by a TV screen, he wished he could just press the mute button to make
them shut up.

### 2.4 Potato seller

You can hear a distant echo shouting "POTATOOOOOES!"  "What's that?",
Seba asked his Grandma.  "That's the potato seller. He sells potatoes
and buys children.  He'll come and take you if you keep being a
naughty boy!", replied Grandma.  Seba never saw the potato seller in
person, but imagined a man with no face wearing a black hood, pushing
him into a linen potato bag. Several years later the potato seller
stopped showing up, nevertheless Seba kept hearing the faint echo as
if the faceless man was still lurking somewhere in the city.

### 2.5 Chimera

### 2.6 Road sign monster

### 2.7 Konstal 105N

### 2.8 The queen of street lights

### 2.9 Mr Shadow

## 3. Characters

### 3.1 Sebastian (Seba)

Theme: "Like a dream about another person's life. Who even am I, is
this where I belong?"

Seba is a man who freshly entered adulthood, a Polish wigga with
simple values and goals. Seba is a patriot - he loves his country and
neighborhood, he feels one with concrete. Not too talkative, thinks
first then speaks. Seba carefully watches his environment and acts
accordingly to contain any shitstorm before it happens. Was not too
popular in school, but did not make many foes either. Liked by his
close buddy Mariusz who often invites Seba to hang out with his crew
to drink, smoke and hit on hoes. Seba is loyal to his girlfriend and
would do everything for her.

"But is this really who I am?"

Seba's room is filled with many books: "Making Beer at Home for
Beginners", "Ontology of Everything" (unknown author), "A myth about
creation of the world in 7.5 days", "Everything About Concrete",
"Ferdydurke", "Goat Anatomy" and "The Selfish Gene".  Seba is
surprisingly intelligent for who he is, curiosity often leads him to
strange places. "What the fuck are you reading, Seba?" asked
Mariusz, "Be careful or you'll become an egghead ehehehe". "I found
these books next to a trashcan, they look good enough to sell them" he
replied, "Don't worry about it too much".

"Am I lying to him or myself?"

Not too religious, the John Paul 2 portrait he got from his parents
ended up in the drawer. "Do you have faith in God, Sebastian?" is a
question he often hears from his relatives. "Yes, don't worry,
Grandma".

"Is there a god?"

Seba likes taking walks around his neighborhood to check if concrete
is still there. The smell of hot asphalt fills his nose as if it was a
fresh mountain breeze. These apartment blocks bring back memories like
a monument to his past self, both the good and the bad moments of his
life. This concrete knows his pain and sees his tears, remembers the
darkest days.

"I hope my neighborhood stays like this forever..."

Quotes:

"Am I fucked up, or is the world fucked up?"

"What the fuck, a talking picture?"

"Copernicus, are you a fag? Your voice is..."

"You may be a fag, but you're also a Great Pole, I trust you, Copernicus!"

"This is my city, my neighborhood. I was born here, don't you get it
Copernicus?"

"I need to find my girlfriend."

"This is insane, this makes no sense. Why does it exist? Why?"

### 3.2 Copernicus (Kopernik)

Theme: "He stopped the Earth and moved the Sun, but he was actually a
German, so for Sebastian both the Earth and the Sun stopped and stars
started falling".

Seba's guide in the Perfect City, just like Virgil for Dante. He
appears to know more than Seba, but mostly gives him only ambiguous
clues.  Seba trusts Kopernik, because he considers him to be a Great
Pole, but is mostly unaware of the fact that Kopernik is also indeed a
German. Kopernik is a mysterious figure touching the divine, always
cold, rarely showing emotions, he also has an unmanly voice of a
dandy.  He first appears in the city in the form of a talking portrait
but then gains a human form as the game progresses.  As Seba discovers
the truth, his trust in Kopernik wanes.

Quotes:

"Would you like some tea? I know you love bisquits."

"What makes you think this isn't the real world?"

"Did you know I was actually a German?"

"I only know what you know, Seba."

"I was keeping the celestial bodies moving for you, I moved the Sun
and stopped the Earth, but I cannot do so any longer"

"If you ever meet my evil twin brother - Claus, don't talk to
him. He's a broken and cynical man and will surely try to deceive
you. Don't play his wicked games, Seba."

### 3.3 Aleks (Ari) Ariadne - Imaginary Girlfriend

Theme: "Like those precious summer days - the blue sky, green trees
and orange sun. Will you stay with me forever?".

Full of energy and sincere, always knows what to say when you feel
down.  "We used to spend so much time together as kids, don't you
remember?"  Loves hanging around with you, will do stupid shit with
you like building a dam and poking a dead bird with a stick.
A nostalgic and warm feeling fills your heart, the feeling in your
guts is love. You wish this summer never ends.

Quotes:

"I'll always be your bro, bro."

"I like you for who you are, Seba."

"Give me your hand!"

"I'm really happy I've met you!"

"You know, I'm a girl after all..."

### 3.4 Aleks (Ari) Ariadne - Imaginary Childhood Bro

Theme: "Like those precious summer days - the blue sky, green trees
and orange sun. Will you stay with me forever?".

Full of energy and sincere, always knows what to say when you feel
down.  "We used to spend so much time together as kids, don't you
remember?"  Loves hanging around with you, will do stupid shit with
you like building a dam and poking a dead bird with a stick.
A nostalgic and warm feeling fills your heart, the feeling in your
guts is love. You wish this summer never ends.

Quotes:

"I'll always be your bro, bro."

"I like you for who you are, Seba."

"Give me your hand!"

"I'm really happy I've met you!"

"N-no, you can't, we're both boys!"
