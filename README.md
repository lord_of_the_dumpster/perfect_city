![Perfect City](doc/logo.png)

Created by lord_of_the_dumpster
for `Minetest 5.9+`

**TRIGGER WARNING: horror, gore, sensitive subjects (suicide, mental illness), bad language**

DISCLAIMER: _This game is a horror game and contains depictions of sensitive subjects such as gore, mental illness and suicide and therefore is not suited for some audiences. The game's target audience are adults._

## About
Perfect City is a psychological horror game about an infinite maze city of a broken mind.
_The project is in early ALPHA._

## Story
The main character, Seba (Sebastian) is a wigga living in Poland. One day he gets a call from his best friend and discovers his girlfriend has been kidnapped!!!
He rushes to save his gf but as he arrives at her place he sees open doors and the apartment is completely empty.
On the walls he sees dirty marks showing where furniture were once. None of that shit makes any sense.
Seba feels dizzy and goes downstairs to get some fresh air and think. As he reaches the outside, he enters the Perfect City.
It's midday, yet the sky is dark-blue, sodium street lights are glowing and the sun is gone.
There's no single person around. "What the fuck is this shit?!?" asked Seba. No one replied but the City listens.
This is the Perfect City, the City where all your dreams and nightmares come true, the City of madness.

## Gameplay
You're trapped in the Perfect City, the maze city of a broken mind.
You need to survive - find food, shelter and avoid getting eaten by the City.
The City knows where you are, stalks you and sends its creatures to kill you.
As you explore the City you can stumble upon Seba's lost memories and discover the truth behind his disappearance.

## Installation
Put the `perfect-city` directory into your `minetest/games` directory.

https://wiki.minetest.net/Games#Installing_games

## Current features
1. **Some nodes**
1. **Character model & skin**
1. **Moody skyline**
1. **Road generation (WIP)**

## Planned features
1. **Randomly generated city**
1. **The fear of getting lost**
1. **Trams & Busses**
1. **Monsters**
1. **Spooky stuff**
1. **Copernicus**
1. **Villains**
1. **Story and getting "out"**

## Software Freedom
Perfect City is free software as defined here: https://www.gnu.org/philosophy/free-sw.html

So "it means that the users have the freedom to run, copy, distribute, study, change and improve the software".

No spyware, etc.

## Contributing
Right now the repository is hosted at Codeberg: https://codeberg.org/lord_of_the_dumpster/perfect_city

I have a strict artistic vision of what I want this game to be, see `Perfect_City_Design_Guide.md` for more info.
Ask me before putting work into coding something to confirm that's something I want in Perfect City. If it doesn't fit, you're free to maintain your own fork.

Some general guidelines:
- the City should have post-soviet, Central/Eastern Europe aesthetics because Seba is a Pole
- the timeline is vaguely around 2000s and 2010s
- monsters should be elements of the city that became alive, e.g. anthropomorphic road signs, pipes, electric stuff, ovens, etc. Other monster types are possible for consideration.
- elements of surrealism and madness are welcome, e.g. disappearing stuff, melting reality, roads leading to nowhere
- there are few NPCs planned, mostly Copernicus (the guide), a villain that embodies Seba's traumas, a weird Granny, a mysterious girl. I don't plan more NPCs than these, because the Perfect City is mostly devoid of humans
- stuff should be dark and moody
- avoid cliché/cringe stuff
- the game is not Minecraft, nor its lookalike

## FAQ

> Was this inspired by the backrooms?

No, I got the idea of a maze city with no people around 2013. I slowly developed it over years in my head and only recently stumbled upon the backrooms.
I'm a huge fan of liminal spaces though and seeing the backrooms for Minetest motivated me to do my game in Minetest.
The only idea I got from the other game is upscaling the player model to make the scale of everything appear more natural.

> Are you insane?

Yes.

> Can I make this game proprietary?

Yeah, just pay me $500 million. I accept payments in gold and silver too.

> Why not GitHub?

GitHub is proprietary software and proprietary software is often malware.

https://www.gnu.org/proprietary/proprietary.html

## Thanks!

Thanks to Mantar, Izzyb, Meniptah and other people who taught me minetest modding.

Thanks to Minetest devs for making Minetest.

Thanks to people who published their minetest mods/games under free software licenses.

Thanks to people who thanklessly maintain libre software I use.
